const data = [
  {
    nama_mk: "Pengantar Aplikasi Komputer",
    total_dibuka: 75,
    total_terisi: 60
  },
  {
    nama_mk: "Statistik",
    total_dibuka: 35,
    total_terisi: 30
  },
  {
    nama_mk: "Akuntansi Sektor Publik",
    total_dibuka: 110,
    total_terisi: 91
  },
  {
    nama_mk: "Akuntansi Manajemen",
    total_dibuka: 95,
    total_terisi: 70
  },
  {
    nama_mk: "Akuntansi Keuangan",
    total_dibuka: 30,
    total_terisi: 25
  },
  {
    nama_mk: "Pengantar Bisnis & Manajemen",
    total_dibuka: 130,
    total_terisi: 130
  },
  {
    nama_mk: "Perpajakan",
    total_dibuka: 265,
    total_terisi: 229
  },
  {
    nama_mk: "Dasar-dasar Manajemen ",
    total_dibuka: 70,
    total_terisi: 60
  },
  {
    nama_mk: "Pengantar Bisnis",
    total_dibuka: 190,
    total_terisi: 169
  },
  {
    nama_mk: "Pengantar Ekonomi",
    total_dibuka: 195,
    total_terisi: 175
  },
  {
    nama_mk: "Hukum Bisnis ",
    total_dibuka: 70,
    total_terisi: 60
  },
  {
    nama_mk: "Perilaku Organisasional",
    total_dibuka: 70,
    total_terisi: 70
  },
  {
    nama_mk: "Enterpreneurship",
    total_dibuka: 760,
    total_terisi: 760
  },
  {
    nama_mk: "Komunikasi Lintas Budaya",
    total_dibuka: 135,
    total_terisi: 119
  },
  {
    nama_mk: "Komunikasi Bisnis",
    total_dibuka: 235,
    total_terisi: 173
  },
  {
    nama_mk: "Lembaga Keuangan & Pasar Modal",
    total_dibuka: 70,
    total_terisi: 50
  },
  {
    nama_mk: "Leadership & Team Development",
    total_dibuka: 15,
    total_terisi: 15
  },
  {
    nama_mk: "Psikologi Olahraga",
    total_dibuka: 40,
    total_terisi: 18
  },
  {
    nama_mk: "Pengantar Psikologi",
    total_dibuka: 140,
    total_terisi: 140
  },
  {
    nama_mk: "Organizational Behavior",
    total_dibuka: 315,
    total_terisi: 306
  },
  {
    nama_mk: "Self and Identity",
    total_dibuka: 150,
    total_terisi: 150
  },
  {
    nama_mk: "Psikologi Kognitif",
    total_dibuka: 305,
    total_terisi: 275
  },
  {
    nama_mk: "Urban Psychology",
    total_dibuka: 165,
    total_terisi: 160
  },
  {
    nama_mk: "Consumer Behaviour",
    total_dibuka: 210,
    total_terisi: 201
  },
  {
    nama_mk: "Manajemen SDM",
    total_dibuka: 60,
    total_terisi: 51
  },
  {
    nama_mk: "Psikologi Bermain",
    total_dibuka: 30,
    total_terisi: 30
  },
  {
    nama_mk: "Psikologi Komunitas",
    total_dibuka: 170,
    total_terisi: 170
  },
  {
    nama_mk: "Psikologi Lintas Budaya",
    total_dibuka: 165,
    total_terisi: 165
  },
  {
    nama_mk: "Psikologi Dalam Kehidupan Sehari-Hari",
    total_dibuka: 265,
    total_terisi: 262
  },
  {
    nama_mk: "Hidup Dalam Lintas Budaya",
    total_dibuka: 90,
    total_terisi: 90
  },
  {
    nama_mk: "Komunitas Perkotaan",
    total_dibuka: 216,
    total_terisi: 215
  },
  {
    nama_mk: "Perilaku Sosial Menyimpang",
    total_dibuka: 45,
    total_terisi: 45
  },
  {
    nama_mk: "Gender, Kerja dan Tempat Kerja",
    total_dibuka: 46,
    total_terisi: 46
  },
  {
    nama_mk: "Psikologi Industri & Organisasi",
    total_dibuka: 60,
    total_terisi: 40
  },
  {
    nama_mk: "Media Promosi Wirausaha",
    total_dibuka: 176,
    total_terisi: 175
  },
  {
    nama_mk: "Pengantar Ilmu Komunikasi",
    total_dibuka: 225,
    total_terisi: 221
  },
  {
    nama_mk: "Perkembangan Teknologi Informasi & Komunikasi                 ",
    total_dibuka: 115,
    total_terisi: 115
  },
  {
    nama_mk: "Komunikasi & Perilaku Manusia",
    total_dibuka: 270,
    total_terisi: 267
  },
  {
    nama_mk: "Media Audio Visual ",
    total_dibuka: 240,
    total_terisi: 236
  },
  {
    nama_mk: "Public Speaking ",
    total_dibuka: 180,
    total_terisi: 120
  },
  {
    nama_mk: "Gambar Teknik",
    total_dibuka: 30,
    total_terisi: 6
  },
  {
    nama_mk: "Teknik Presentasi",
    total_dibuka: 30,
    total_terisi: 10
  },
  {
    nama_mk: "Material Produk",
    total_dibuka: 155,
    total_terisi: 124
  },
  {
    nama_mk: "Modelling & Prototyping",
    total_dibuka: 150,
    total_terisi: 130
  },
  {
    nama_mk: "Green Desain",
    total_dibuka: 55,
    total_terisi: 55
  },
  {
    nama_mk: "Creativepreneurship",
    total_dibuka: 60,
    total_terisi: 52
  },
  {
    nama_mk: "Urban Product Design Review",
    total_dibuka: 145,
    total_terisi: 130
  },
  {
    nama_mk: "Urban Toy Design",
    total_dibuka: 130,
    total_terisi: 93
  },
  {
    nama_mk: "Desain Kemasan",
    total_dibuka: 195,
    total_terisi: 153
  },
  {
    nama_mk: "Character & Custome Design",
    total_dibuka: 70,
    total_terisi: 62
  },
  {
    nama_mk: "Eksibisi Display Desain",
    total_dibuka: 130,
    total_terisi: 86
  },
  {
    nama_mk: "Urban Merchandise Product Design ",
    total_dibuka: 105,
    total_terisi: 92
  },
  {
    nama_mk: "Ergonomic Product Design",
    total_dibuka: 130,
    total_terisi: 96
  },
  {
    nama_mk: "HAKI Desain",
    total_dibuka: 480,
    total_terisi: 409
  },
  {
    nama_mk: "Kritik Desain",
    total_dibuka: 73,
    total_terisi: 68
  },
  {
    nama_mk: "Urban Craft Design",
    total_dibuka: 155,
    total_terisi: 109
  },
  {
    nama_mk: "Creative Urban Culture",
    total_dibuka: 320,
    total_terisi: 287
  },
  {
    nama_mk: "Product Sketching and Rendering",
    total_dibuka: 45,
    total_terisi: 16
  },
  {
    nama_mk: "Urban Fashion & Lifestyle Product Design",
    total_dibuka: 155,
    total_terisi: 130
  },
  {
    nama_mk: "Prinsip Rekayasa Produk",
    total_dibuka: 45,
    total_terisi: 32
  },
  {
    nama_mk: "Workshop Kayu & Metal",
    total_dibuka: 90,
    total_terisi: 90
  },
  {
    nama_mk: "Interactive Product Design Presentation",
    total_dibuka: 15,
    total_terisi: 12
  },
  {
    nama_mk: "Workshop Logam/Desain Aksesoris Fashion",
    total_dibuka: 30,
    total_terisi: 30
  },
  {
    nama_mk: "Desain Sarana Transportasi",
    total_dibuka: 150,
    total_terisi: 150
  },
  {
    nama_mk: "Eksibisi & Display Desain",
    total_dibuka: 30,
    total_terisi: 20
  },
  {
    nama_mk: "Fotografi Dasar",
    total_dibuka: 130,
    total_terisi: 100
  },
  {
    nama_mk: "Fotografi Periklanan",
    total_dibuka: 30,
    total_terisi: 11
  },
  {
    nama_mk: "Ilustrasi Buku Anak",
    total_dibuka: 15,
    total_terisi: 3
  },
  {
    nama_mk: "Program Video Iklan TV",
    total_dibuka: 30,
    total_terisi: 17
  },
  {
    nama_mk: "Desain Infografis",
    total_dibuka: 105,
    total_terisi: 99
  },
  {
    nama_mk: "Rupa 2D",
    total_dibuka: 60,
    total_terisi: 60
  },
  {
    nama_mk: "Pengantar Studi Seni Rupa & Desain",
    total_dibuka: 105,
    total_terisi: 104
  },
  {
    nama_mk: "Proses Kreatif",
    total_dibuka: 130,
    total_terisi: 120
  },
  {
    nama_mk: "Komputer Multimedia",
    total_dibuka: 30,
    total_terisi: 29
  },
  {
    nama_mk: "Fotografi",
    total_dibuka: 15,
    total_terisi: 10
  },
  {
    nama_mk: "Desain Infografis",
    total_dibuka: 60,
    total_terisi: 55
  },
  {
    nama_mk: "Komunikasi Media & Masyarakat",
    total_dibuka: 212,
    total_terisi: 202
  },
  {
    nama_mk: "Web Design",
    total_dibuka: 260,
    total_terisi: 224
  },
  {
    nama_mk: "Pengantar Desain",
    total_dibuka: 181,
    total_terisi: 181
  },
  {
    nama_mk: "Statistika dan Probabilitas",
    total_dibuka: 40,
    total_terisi: 11
  },
  {
    nama_mk: "Aljabar Linier",
    total_dibuka: 30,
    total_terisi: 8
  },
  {
    nama_mk: "Perancangan & Pemrograman Web",
    total_dibuka: 30,
    total_terisi: 11
  },
  {
    nama_mk: "Algoritma Pemrograman",
    total_dibuka: 55,
    total_terisi: 50
  },
  {
    nama_mk: "Jaringan Komputer",
    total_dibuka: 85,
    total_terisi: 60
  },
  {
    nama_mk: "Desain Web",
    total_dibuka: 85,
    total_terisi: 77
  },
  {
    nama_mk: "Sistem Basis Data",
    total_dibuka: 85,
    total_terisi: 64
  },
  {
    nama_mk: "Sistem Digital",
    total_dibuka: 160,
    total_terisi: 144
  },
  {
    nama_mk: "Komputer & Masyarakat",
    total_dibuka: 280,
    total_terisi: 268
  },
  {
    nama_mk: "Technopreneurship",
    total_dibuka: 118,
    total_terisi: 96
  },
  {
    nama_mk: "Pengantar Kecerdasan Buatan",
    total_dibuka: 60,
    total_terisi: 60
  },
  {
    nama_mk: "Interaksi Manusia Komputer",
    total_dibuka: 205,
    total_terisi: 185
  },
  {
    nama_mk: "Pengantar Keamanan Siber",
    total_dibuka: 66,
    total_terisi: 66
  },
  {
    nama_mk: "Sistem Informasi Manajemen",
    total_dibuka: 130,
    total_terisi: 110
  },
  {
    nama_mk: "Perancangan & Pemrograman Web",
    total_dibuka: 30,
    total_terisi: 8
  },
  {
    nama_mk: "Teknik Aplikasi Multimedia",
    total_dibuka: 45,
    total_terisi: 31
  },
  {
    nama_mk: "Fondasi Pemrograman & Struktur Data  ",
    total_dibuka: 80,
    total_terisi: 80
  },
  {
    nama_mk: "Aljabar Linier",
    total_dibuka: 75,
    total_terisi: 70
  },
  {
    nama_mk: "Knowledge Management",
    total_dibuka: 155,
    total_terisi: 137
  },
  {
    nama_mk: "Komputer & Masyarakat",
    total_dibuka: 100,
    total_terisi: 85
  },
  {
    nama_mk: "E-Commerce ",
    total_dibuka: 270,
    total_terisi: 240
  },
  {
    nama_mk: "Sistem Basis Data",
    total_dibuka: 120,
    total_terisi: 118
  },
  {
    nama_mk: "Pengantar Sistem Informasi",
    total_dibuka: 255,
    total_terisi: 249
  },
  {
    nama_mk: "Customer Relationship Management",
    total_dibuka: 45,
    total_terisi: 23
  },
  {
    nama_mk: "Sistem Informasi Akuntansi & Keuangan",
    total_dibuka: 45,
    total_terisi: 31
  },
  {
    nama_mk: "Pengajaran Berbantuan Komputer",
    total_dibuka: 45,
    total_terisi: 35
  },
  {
    nama_mk: "Kalkulus",
    total_dibuka: 15,
    total_terisi: 15
  },
  {
    nama_mk: "Ilmu Dasar Sains",
    total_dibuka: 30,
    total_terisi: 30
  },
  {
    nama_mk: "Pengantar Ilmu Teknik Sipil",
    total_dibuka: 245,
    total_terisi: 245
  },
  {
    nama_mk: "Menggambar Rekayasa",
    total_dibuka: 30,
    total_terisi: 30
  },
  {
    nama_mk: "Sustainable Eco Development (SED)  ",
    total_dibuka: 665,
    total_terisi: 605
  },
  {
    nama_mk: "Fisika Bangunan",
    total_dibuka: 90,
    total_terisi: 78
  },
  {
    nama_mk: "Kritik Arsitektur",
    total_dibuka: 30,
    total_terisi: 0
  },
  {
    nama_mk: "Arsitektur Digital",
    total_dibuka: 30,
    total_terisi: 12
  },
  {
    nama_mk: "Pengantar Arsiterktur",
    total_dibuka: 60,
    total_terisi: 60
  },
  {
    nama_mk: "Gambar Bentuk",
    total_dibuka: 60,
    total_terisi: 60
  },
  {
    nama_mk: "Arsitektur Modern",
    total_dibuka: 60,
    total_terisi: 60
  },
  {
    nama_mk: "Pranata Pembangunan",
    total_dibuka: 160,
    total_terisi: 149
  },
  {
    nama_mk: "Arsitektur & Perilaku",
    total_dibuka: 90,
    total_terisi: 90
  },
  {
    nama_mk: "Fotografi Arsitektur",
    total_dibuka: 30,
    total_terisi: 30
  },
  {
    nama_mk: "Perancangan Ruang Luar (Eksterior Design)",
    total_dibuka: 30,
    total_terisi: 30
  },
  {
    nama_mk: "Arsitektur Hijau",
    total_dibuka: 137,
    total_terisi: 136
  },
  {
    nama_mk: "Perancangan Ruang Dalam (Interior Design)",
    total_dibuka: 85,
    total_terisi: 77
  },
  {
    nama_mk: "Arsitektur Modelling",
    total_dibuka: 160,
    total_terisi: 129
  },
  {
    nama_mk: "Manajemen Konstruksi",
    total_dibuka: 60,
    total_terisi: 50
  },
  {
    nama_mk: "Rencana Anggaran Biaya",
    total_dibuka: 30,
    total_terisi: 20
  },
  {
    nama_mk: "Software (Digital) Arsitektur",
    total_dibuka: 30,
    total_terisi: 20
  },
  {
    nama_mk: "Statistika Bisnis",
    total_dibuka: 30,
    total_terisi: 20
  },
  {
    nama_mk: "Sistem Basis Data",
    total_dibuka: 560,
    total_terisi: 512
  }
];

let vectors = new Array();

// console.log(data);
for (let i = 0; i < data.length; i++) {
  vectors[i] = [data[i]["total_dibuka"], data[i]["total_terisi"]];
}

function sortWithIndeces(toSort) {
  for (var i = 0; i < toSort.length; i++) {
    toSort[i] = [toSort[i], i];
  }
  toSort.sort(function(left, right) {
    return left[0] < right[0] ? -1 : 1;
  });
  toSort.sortIndices = [];
  for (var j = 0; j < toSort.length; j++) {
    toSort.sortIndices.push(toSort[j][1]);
    toSort[j] = toSort[j][0];
  }
  return toSort;
}

// console.log(vectors);

const kmeans = require("node-kmeans");
const result = kmeans.clusterize(vectors, { k: 3 }, (err, res) => {
  if (err) console.error(err);
  else {
    return res;
    // const clusterOne = res[0].centroid.reduce((previous, current) => {
    //   // console.log(previous);
    //   // console.log(current);
    //   return previous + current;
    // });

    // res[0]["total_centroid"] = clusterOne;

    // const clusterTwo = res[1].centroid.reduce((previous, current) => {
    //   // console.log(previous);
    //   // console.log(current);
    //   return previous + current;
    // });

    // res[1]["total_centroid"] = clusterTwo;

    // const clusterThree = res[2].centroid.reduce((previous, current) => {
    //   // console.log(previous);
    //   // console.log(current);
    //   return previous + current;
    // });

    // res[2]["total_centroid"] = clusterThree;

    // const sortArr = res.sort(function(a, b) {
    //   var keyA = a.total_centroid,
    //     keyB = b.total_centroid;
    //   // Compare the 2 dates
    //   if (keyA < keyB) return -1;
    //   if (keyA > keyB) return 1;
    //   return 0;
    // });

    // let kurangDiminati = [];

    // // Kurang dinikmati
    // // console.log(sortArr[0].clusterInd);
    // sortArr[0].clusterInd.forEach(value => {
    //   data[value].keterangan = "Kurang Diminati";
    //   kurangDiminati.push(data[value]);
    // });

    // console.log(kurangDiminati);

    // let cukupDiminati = [];

    // sortArr[1].clusterInd.forEach(value => {
    //   data[value].keterangan = "Cukup Diminati";
    //   cukupDiminati.push(data[value]);
    // });

    // console.log(cukupDiminati);

    // let banyakDiminati = [];

    // sortArr[2].clusterInd.forEach(value => {
    //   data[value].keterangan = "Banyak Diminati";
    //   banyakDiminati.push(data[value]);
    // });

    // console.log(banyakDiminati);

    // let matkulMerge = [...kurangDiminati, ...cukupDiminati, ...banyakDiminati];
  }
});

export default result;

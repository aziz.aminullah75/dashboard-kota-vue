import { mapState } from "vuex";

export const planetChartData = {
  type: "bar",
  data: {
    labels: [
      "Mata Kuliah 1",
      "Mata Kuliah 2",
      "Mata Kuliah 3",
      "Mata Kuliah 4",
      "Mata Kuliah 5",
      "Mata Kuliah 6",
      "Mata Kuliah 7",
      "Mata Kuliah 8",
      "Mata Kuliah 9",
      "Mata Kuliah 10"
    ],
    datasets: [
      {
        label: "Jumlah Kuota Dibuka",
        data: this.kmeansList.data.map(value => value.total_dibuka),
        backgroundColor: "rgba(30, 35, 107,.5)",
        borderColor: "#161a4f",
        borderWidth: 2
      },
      {
        label: "Jumlah Mahasiswa",
        data: [100, 90, 87, 80, 79, 75, 70, 70, 65, 60],
        backgroundColor: "rgba(71, 183,132,.5)",
        borderColor: "#47b784",
        borderWidth: 2
      }
    ]
  },
  options: {
    responsive: true,
    lineTension: 1,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            padding: 25
          }
        }
      ]
    }
  }
};

export default planetChartData;

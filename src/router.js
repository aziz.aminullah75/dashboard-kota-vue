import Vue from "vue";
import Router from "vue-router";
import Index from "./views/Index.vue";
import store from "./services/store";
// import MainNavbar from "./layout/MainNavbar.vue";
import Sidebar from "./layout/Sidebar.vue";
import LoginNavbar from "./layout/LoginNavbar.vue";
import MainFooter from "./layout/MainFooter.vue";
import SideFooter from "./layout/SideFooter.vue";
import BlankFooter from "./layout/BlankFooter.vue";
import BlankNavbar from "./layout/BlankNavbar.vue";
// import Landing from "./views/Landing.vue";
// import Profile from "./views/Profile.vue";
// import Demo from "./views/Demo.vue";

import Login from "./views/pages/Login.vue";

import Dashboard from "./views/dashboard/Dashboard.vue";
import UsersManagement from "./views/dashboard/user/UsersManagement.vue";
import UsersUpdate from "./views/dashboard/user/UsersUpdate.vue";
import UsersCreate from "./views/dashboard/user/UsersCreate.vue";

import MataKuliah from "./views/dashboard/mata_kuliah/MataKuliah.vue";
import MataKuliahCreate from "./views/dashboard/mata_kuliah/MataKuliahCreate.vue";
import MataKuliahUpdate from "./views/dashboard/mata_kuliah/MataKuliahUpdate.vue";
import ReportMataKuliah from "./views/dashboard/mata_kuliah/report/ReportMataKuliah.vue";
import ReportMataKuliahCreate from "./views/dashboard/mata_kuliah/report/ReportMataKuliahCreate.vue";
import ReportMataKuliahUpdate from "./views/dashboard/mata_kuliah/report/ReportMataKuliahUpdate.vue";
import ReportMataKuliahKaprodi from "./views/dashboard/mata_kuliah/kaprodi/ReportMataKuliahKaprodi.vue";
import ReportMataKuliahKaprodiCreate from "./views/dashboard/mata_kuliah/kaprodi/ReportMataKuliahKaprodiCreate.vue";
import ReportMataKuliahKaprodiUpdate from "./views/dashboard/mata_kuliah/kaprodi/ReportMataKuliahKaprodiUpdate.vue";
import BookingPriority from "./views/dashboard/mata_kuliah/booking/BookingMataKuliah.vue";
import BookingPriorityCreate from "./views/dashboard/mata_kuliah/booking/BookingMataKuliahCreate.vue";
import BookingPriorityUpdate from "./views/dashboard/mata_kuliah/booking/BookingMataKuliahUpdate.vue";
import BookingPriorityUpdateAdmin from "./views/dashboard/mata_kuliah/booking/BookingMataKuliahUpdate2.vue";
import PeriodeCreate from "./views/dashboard/periode/PeriodeCreate.vue";
import PeriodeUpdate from "./views/dashboard/periode/PeriodeUpdate.vue";
import Dosen from "./views/dashboard/dosen/Dosen.vue";
import DosenCreate from "./views/dashboard/dosen/DosenCreate.vue";
import DosenUpdate from "./views/dashboard/dosen/DosenUpdate.vue";

import Jadwal from "./views/dashboard/jadwal/Jadwal.vue";
import JadwalUpdate from "./views/dashboard/jadwal/JadwalUpdate.vue";
import JadwalCreate from "./views/dashboard/jadwal/JadwalCreate.vue";

Vue.use(Router);

export default new Router({
  routes: [
    // {
    //   path: "/",
    //   name: "index",
    //   components: { default: Index, header: MainNavbar, footer: MainFooter },
    //   props: {
    //     header: { colorOnScroll: 400 },
    //     footer: { backgroundColor: "black" }
    //   }
    // },
    {
      path: "/",
      name: "login",
      components: { default: Login, header: LoginNavbar, footer: MainFooter },
      beforeEnter: (to, from, next) => {
        if (store.state.auth.isAuthenticated) {
          next("/dashboard");
        } else {
          next();
        }
      },
      props: {
        header: { colorOnScroll: 400 }
      }
    },

    {
      path: "/dashboard",
      name: "dashboard",
      components: { default: Dashboard, header: Sidebar, footer: SideFooter },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/users-management",
      name: "users-management",
      components: {
        default: UsersManagement,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/users-create",
      name: "users-create",
      components: {
        default: UsersCreate,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/users-update/:id",
      name: "users-update",
      components: {
        default: UsersUpdate,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/mata-kuliah-kota",
      name: "mata-kuliah-kota",
      components: {
        default: MataKuliah,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/mata-kuliah-create",
      name: "mata-kuliah-create",
      components: {
        default: MataKuliahCreate,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/mata-kuliah-update/:id",
      name: "mata-kuliah-update",
      components: {
        default: MataKuliahUpdate,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/report-mata-kuliah-kota",
      name: "report-mata-kuliah-kota",
      components: {
        default: ReportMataKuliah,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/report-mata-kuliah-create",
      name: "report-mata-kuliah-create",
      components: {
        default: ReportMataKuliahCreate,
        header: BlankNavbar,
        footer: BlankFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/report-mata-kuliah-update/:id",
      name: "report-mata-kuliah-update",
      components: {
        default: ReportMataKuliahUpdate,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/report-mata-kuliah-kaprodi",
      name: "report-mata-kuliah-kaprodi",
      components: {
        default: ReportMataKuliahKaprodi,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/mata-kuliah-kaprodi-create",
      name: "mata-kuliah-kaprodi-create",
      components: {
        default: ReportMataKuliahKaprodiCreate,
        header: BlankNavbar,
        footer: BlankFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/mata-kuliah-kaprodi-update/:id",
      name: "mata-kuliah-kaprodi-update",
      components: {
        default: ReportMataKuliahKaprodiUpdate,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/booking-priority",
      name: "booking-priority",
      components: {
        default: BookingPriority,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/booking-priority-create",
      name: "booking-priority-create",
      components: {
        default: BookingPriorityCreate,
        header: BlankNavbar,
        footer: BlankFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/booking-priority-update/:id",
      name: "booking-priority-update",
      components: {
        default: BookingPriorityUpdate,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/booking-priority-update-admin/:id",
      name: "booking-priority-update-admin",
      components: {
        default: BookingPriorityUpdateAdmin,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/manajemen-periode",
      name: "manajemen-periode",
      components: {
        default: PeriodeCreate,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/periode-update/:id",
      name: "periode-update",
      components: {
        default: PeriodeUpdate,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/manajemen-dosen",
      name: "manajemen-dosen",
      components: {
        default: Dosen,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/dosen-create",
      name: "create-dosen",
      components: {
        default: DosenCreate,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/dosen-update/:id",
      name: "dosen-update",
      components: {
        default: DosenUpdate,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/jadwal",
      name: "jadwal",
      components: {
        default: Jadwal,
        header: Sidebar,
        footer: SideFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/jadwal-create",
      name: "jadwal-create",
      components: {
        default: JadwalCreate,
        header: LoginNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard/jadwal-update/:id",
      name: "jadwal-update",
      components: {
        default: JadwalUpdate,
        header: LoginNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    }
    // {
    //   path: "/demo",
    //   name: "demo",
    //   components: { default: Demo, header: MainNavbar, footer: MainFooter },
    //   props: {
    //     header: { colorOnScroll: 400 },
    //     footer: { backgroundColor: "black" }
    //   }
    // },
    // {
    //   path: "/landing",
    //   name: "landing",
    //   components: { default: Landing, header: MainNavbar, footer: MainFooter },
    //   props: {
    //     header: { colorOnScroll: 400 },
    //     footer: { backgroundColor: "black" }
    //   }
    // },
  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});

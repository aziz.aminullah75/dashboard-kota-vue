import DropDown from "./Dropdown.vue";
import Parallax from "./Parallax.vue";
import Pagination from "./Pagination.vue";
import Slider from "./Slider.vue";
import Badge from "./Badge.vue";
import NavTabsCard from "./cards/NavTabsCard.vue";
import LoginCard from "./cards/LoginCard.vue";
import Tabs from "./Tabs.vue";
import Modal from "./Modal.vue";
import PlanetChart from "./PlanetChart.vue";
import PlanetChart2 from "./PlanetChart2.vue";
import Kmeans from "./Kmeans.vue";

export {
  DropDown,
  Parallax,
  Pagination,
  Slider,
  Badge,
  NavTabsCard,
  LoginCard,
  Tabs,
  Modal,
  PlanetChart,
  PlanetChart2,
  Kmeans
};

import ApiService from "../../api.service";
const state = {
  dosenList: [],
  dosenData: {},
  kaprodiDosenList: []
};
const mutations = {
  setDosenList(state, payload) {
    state.dosenList = payload;
  },
  setDosenData(state, payload) {
    state.dosenData = payload;
  },
  setKaprodiDosenList(state, payload) {
    state.kaprodiDosenList = payload;
  }
};
const actions = {
  async getDosenList(context, { prodi_id } = { prodi_id: "" }) {
    try {
      let params = new URLSearchParams();
      if (prodi_id != "") {
        params.append("prodi_id", prodi_id);
      }
      let response = await ApiService.query("api/kota/dosen?" + params);
      context.commit("setDosenList", response.data);
    } catch (error) {
      // console.log(error);
      throw error;
    }
  },
  async createDosen(context, { payload }) {
    try {
      await ApiService.post("api/kota/dosen/create", payload);
    } catch (error) {
      throw Error(error);
    }
  },
  async getDosen(context, { id }) {
    try {
      let response = await ApiService.query(`api/kota/dosen/get/${id}`);
      context.commit("setDosenData", response.data);
    } catch (error) {
      throw Error(error);
    }
  },

  async updateDosen(context, { id, payload }) {
    try {
      await ApiService.post(`api/kota/dosen/update/${id}`, payload);
    } catch (error) {
      throw Error(error);
    }
  },

  async kaprodiDosen(context) {
    try {
      let response = await ApiService.post("api/kota/dosen/kaprodi-dosen");
      context.commit("setKaprodiDosenList", response.data);
    } catch (error) {
      // console.log(error);
      throw error;
    }
  },

  async deleteDosen(context, { id }) {
    try {
      await ApiService.delete(`api/kota/dosen/delete/${id}`);
    } catch (error) {
      throw Error(error);
    }
  }
};
const getters = {};

const dosen = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};

export default dosen;

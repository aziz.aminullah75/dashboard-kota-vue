import Vue from "vue";
import Vuex from "vuex";

import auth from "./auth.module";
import user from "./user.module";

import homeGallery from "./design/home_gallery";
import tema from "./design/tema";

import urban from "./tentang_upj/urban.module";
import prodi from "./tentang_upj/prodi.module";

import periode from "./kota/periode.module";
import jadwal from "./kota/jadwal.module";
import mataKuliah from "./kota/mata_kuliah.module";
import dosen from "./kota/dosen.module";
import laporan from "./kota/laporan.module";
import statistik from "./kota/statistik.module";
import bookingPriority from "./kota/booking_priority.module";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    user,
    periode,
    jadwal,
    mataKuliah,
    dosen,
    laporan,
    statistik,
    urban,
    prodi,
    bookingPriority
  }
});
